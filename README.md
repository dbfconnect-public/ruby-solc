ruby-solc
=========

A Docker image with Ruby, tools to build native extensions
and Ethereum `solc`, the Solidity compiler.

The image is used to test smart contracts using cucumber and
[`ethereum.rb`](https://github.com/EthWorks/ethereum.rb) in
Continuous Integration.
