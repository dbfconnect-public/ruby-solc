FROM ruby:2.5-slim

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y wget gcc make ruby-dev \
  && wget -q -O /usr/local/bin/solc https://github.com/ethereum/solidity/releases/download/v0.4.24/solc-static-linux \
  && chmod a+x /usr/local/bin/solc \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
